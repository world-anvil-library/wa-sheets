// bridge b/w app.js and the rendered dom
import { YAML } from "../node_modules/yaml/browser/dist/index.js";

// YAML takes "required" which could be mapped to input.required
console.log("------------------------------------------");
// select functions

function $$ (selector) {
    return document.querySelector(selector);
};

function $$$ (selector) {
    return document.querySelectorAll(selector);
};

// map input element to yaml entry

const nodeList = [...$$$('input')];
function generateBody (inputCollection) {
    let output = {};
    let list = {};
    inputCollection.forEach(elt => {
        list[elt.name] = formatter(elt);
        // let str = YAML.stringify(eltObj);
        // console.log(str);
    });
    output["fields"] = list;
    return output;
};
let yamloutput = YAML.stringify(generateBody(nodeList));
console.log(yamloutput);

function formatter ({name, type}) {
    // the name attr maps to the label in the output YAML
    // YAML needs an input that maps to input.type attr
    return  {
        label: name,
        input: typeParse(type)
    };
};

function typeParse (type) {
    // TYPES:
    // number => integer
    // text => string

    let output = "";
    switch (type){
        case "text":
            output = "string";
            break;
        case "number":
            output = "integer";
            break;
    }
    return output;
}

// output to app.js for saving to disk