const YAML = require("yaml");

// host static files
// convert sheet to YAML
// convert sheet to display template
// convert sheet to block

// consume raw YAML file and write to disk.

// Test code, please ignore

const thing = {
    name : {
        first : "Your",
        last : "Mom"
    },
    array : ["thing1", "thing2", "thing3"],
    boolean : !!1,
};

const otherThing = YAML.stringify(thing);

console.log(otherThing);